#include <iostream>
#include <string>
#include <iomanip>
#include <ctime>
#include <cstdlib>
#include <windows.h> //for the use of Sleep() in tutorial only.
#include <algorithm> //for random_shuffle only.
using namespace std;

class BurgerList
{
public:
    void setInfo(string i_name, string i_ingredients, int i_cookingtime) {
        name = i_name;
        ingredients = i_ingredients;
        cookingtime = i_cookingtime;
    }

    string getName(){
        return name;

    }
    string getIngredient(int mode){ // mode0 = used to check answer, mode1 = display info on menu, mode2 = display info in GAME
        string temp="";
        if (mode == 0)
            return ingredients;
        else if (mode == 1 || mode == 2)
        {
            for (int i = 0; i < ingredients.length(); i++)
            {
                switch (ingredients[i])
                {
                    case 'b':	if (mode == 1)
                            temp += string("bread");
                        else
                            temp += string("[B]read");
                        break;
                    case 'c':	if (mode == 1)
                            temp += string("cheese");
                        else
                            temp += string("[C]heese");
                        break;
                    case 'f':	if (mode == 1)
                            temp += string("beef");
                        else
                            temp += string("bee[F]");
                        break;
                    case 'l':	if (mode == 1)
                            temp += string("lettuce");
                        else
                            temp += string("[L]ettuce");
                        break;
                    case 't':	if (mode == 1)
                            temp += string("tomato");
                        else
                            temp += string("[T]omato");
                        break;
                    case 'm':	if (mode == 1)
                            temp += string("mushroom");
                        else
                            temp += string("[M]ushroom");
                        break;
                    case 'e':	if (mode == 1)
                            temp += string("egg");
                        else
                            temp += string("[E]gg");
                        break;
                    case 's':	if (mode == 1)
                            temp += string("salmon");
                        else
                            temp += string("[S]almon");
                        break;
                }
                if (i != ingredients.length()-1)
                    temp += string(", ");
            }
            return temp;
        }
    }

    int getCookingTime()
    {
        return cookingtime;
    }

    void shuffleKey()
    {
        random_shuffle(ingredients.begin(), ingredients.end());
    }

private:
    string name;
    string ingredients;
    int cookingtime;
};

class OrderList
{
public:
    void setInfo(int bid, string bname, int timelimit, int cooktime)
    {
        id = bid;
        name = bname;
        status = "preparing";
        remaintime = timelimit;
        startingtime = time(0);
        cookingtime = cooktime;
    }

    void setTimelimit(int time) //*************TO BE USED IN TUTORIAL ONLY!!!*************
    {
        remaintime = time;
    }

    string getDetail()
    {
        string info = "";
        info = name + ", " + status + ", " + getFormatedtime();
        return info;
    }

    /*void shuffleKey()
    {
        string temp = "";

    }
    */

    void updateStatus(char mode)
    {
        if (mode == 'c')
        {
            status = "cooking";
            startcookingtime = time(0);
        }
        else
        {
            if (cookingtime <= 0)
                status = "ready to serve";
        }
    }

    void updateTime()
    {
        remaintime -= (time(0)-startingtime);
        startingtime = time(0);
        if (status == "cooking")
        {
            cookingtime -= (time(0)-startcookingtime);
            startcookingtime = time(0);
            updateStatus('z');
        }
    }

    int getTime()
    {
        updateTime();
        return remaintime;
    }

    string getFormatedtime()
    {
        if (remaintime >= 0)
        {
            string temp;
            temp = to_string(remaintime / 60)+ "'" + to_string(remaintime % 60) + "\"";
            return temp;
        }
        else
            return "0'0\"";
    }

    int getBurgerid()
    {
        return id;
    }

    string getStatus()
    {
        if (remaintime > 0)
            return status;
        else
            return "Time out";
    }

private:
    string name, status;
    int id, remaintime, startingtime, cookingtime, startcookingtime;
};

// Global Object
BurgerList Burger[6];

// Global variable
int timelimit = 40, maxorder = 5, doShuffle = 0; //for shuffling the burger key.

// Functions
void orderInitializaion(int &order_no, OrderList Order[])
{
    order_no = 1 + rand() % maxorder;
    for (int i = 1; i <= order_no; i++)
    {
        int random = 1 + rand() % 5;
        Order[i].setInfo(random, Burger[random].getName(), timelimit, Burger[random].getCookingTime());
    }
}

void swapArr(OrderList Order[], int i)
{
    OrderList temp_Order;
    temp_Order = Order[i];
    Order[i] = Order[i+1];
    Order[i+1] = temp_Order;
}

void orderUpdate(int &order_no, int &score, OrderList Order[])
{
    //start of removing time out
    int countEmpty = 0;

    for (int i = 1; i <= order_no; i++)
    {
        Order[i].updateTime();
        if (Order[i].getTime() <= 0)
            countEmpty += 1;
    }

    if (countEmpty != 0)
    {
        score -= countEmpty*5;

        for (int j = 1; j <= order_no; j++)
        {
            if (Order[j].getTime() <= 0 && j != maxorder)
            {
                for (int k = j; k <= order_no-1; k++)
                    swapArr(Order, k);
                order_no -= 1;
            }
            else if (Order[j].getTime() <= 0 && j == maxorder)
                order_no -= 1;
        }
    }
    //end of removing time out;

    //start of randomly generating orders
    if (order_no < maxorder)
    {
        int determination =  1 + rand() % 10;
        if (determination > 8) //20% of chances to add one or more orders
        {
            int add = 0; // how many orders are going to be added.
            add = 1 + rand() % (maxorder - order_no);
            for (int i = 1; i <= add; i++)
            {
                int temp = 1 + rand() % 5;
                order_no += 1;
                Order[order_no].setInfo(temp,Burger[temp].getName(),timelimit, Burger[temp].getCookingTime());
            }
        }
        else if (determination > 5) //50 % chances to add one order only
        {
            int temp = 1 + rand() % 5;
            order_no += 1;
            Order[order_no].setInfo(temp,Burger[temp].getName(),timelimit, Burger[temp].getCookingTime());
        }
    }
    // end of randomly generating orders
}

void printOrderlist(int order_no, int score, OrderList Order[])
{
    cout << "*** Order list ***\n";
    for (int i = 1; i <= order_no; i++)
        cout << "Order #" << i << ": " << Order[i].getDetail() << endl;
    cout << "-----------------------------------------------\n";
    cout << "score: " << score << endl;
}

void printProcessorder(string input, OrderList Order[])
{
    system("cls");
    Order[stoi(input)].updateTime();
    cout << "*** Process Order ***\n";
    cout << left << setw(22) << "Order #" << ": " << input << endl;
    cout << left << setw(22) << "Burger" << ": " << Burger[Order[stoi(input)].getBurgerid()].getName() << endl;
    cout << left << setw(22) << "Status" << ": " << Order[stoi(input)].getStatus() << endl;
    cout << left << setw(22) << "Remaining Time" << ": " << Order[stoi(input)].getFormatedtime() << endl;
    cout << left << setw(22) << "Burger Ingredient List" << ": " << Burger[Order[stoi(input)].getBurgerid()].getIngredient(2) << endl;
    cout << left << setw(22) << "Burger Key List" << ": " << Burger[Order[stoi(input)].getBurgerid()].getIngredient(0) << right << endl << endl;
}

bool Exit()
{
    int valid;
    string confirm="";

    do {
        valid = 1;
        cout << "\n\nAre you going to EXIT?\nAns: ";
        cin >> confirm;

        if (confirm == "y" || confirm == "Y")
            return true;
        else if (confirm == "n" || confirm == "N")
            return false;
        else
        {
            valid = 0;
            cout << "\nInvalid input. Please try again.";
        }
    } while (valid != 1);
}

// Game component
void StartGame()
{
    //initializing the game
    int order_no, score = 10; //order_no = existing number of order.
    string input;
    OrderList Order[maxorder+1]; //use only order[1-maxorder]

    orderInitializaion(order_no, Order);
    //end of initializing

    do
    {
        system("cls");
        printOrderlist(order_no, score, Order);
        cout << "Enter [U] for update, [Q] for Quit, or [1-" << order_no << "] for order: ";

        if (score < 0)
        {
            cout << "\n\nyour score is below 0!\nGame Over!\n";
            system("pause");
            break;
        }
        cin >> input;

        while (!(input == "u" || input == "U") && !(input == "q" || input == "Q") && !(input >= "1" && input <= to_string(order_no)))
        {
            cout << "Invalid input! Please try again!\n";
            cout << "Enter [U] for update, [Q] for Quit, or [1-" << order_no << "] for order: ";
            cin >> input;
        }

        if (input == "q" || input == "Q")
            if (Exit())
                break;

            else if (input >= "1" && input <= to_string(order_no) && Order[stoi(input)].getStatus() == "ready to serve")
            {
                for (int i = stoi(input); i <= order_no-1; i++)
                    swapArr(Order, i);
                order_no -= 1;
                score += 10;
            }

            else if (input >= "1" && input <= to_string(order_no) && Order[stoi(input)].getStatus() == "cooking")
            {
                cout << "This order is still in cookking. Please select other order\n";
                system("pause");
            }

            else if (input >= "1" && input <= to_string(order_no))
            {
                //start of processing order
                string ans = "";
                if (doShuffle == 1)
                    Burger[Order[stoi(input)].getBurgerid()].shuffleKey();
                do
                {
                    printProcessorder(input, Order);
                    cout << "Please choose [U] for update, [R] for return, or\n";
                    cout << "type correct key list to start cooking: ";
                    cin >> ans;

                    if (!(ans == "u" || ans == "U") && !(ans == "r" || ans == "R"))
                    {

                        if (Order[stoi(input)].getTime() > 0 && ans == Burger[Order[stoi(input)].getBurgerid()].getIngredient(0))
                        {
                            Order[stoi(input)].updateStatus('c');
                            ans = "r";
                        }
                        else if (Order[stoi(input)].getTime() <= 0)
                        {
                            cout << "\nTime out!\n";
                            ans = "r";
                            system("pause");
                        }
                        else if (ans != Burger[Order[stoi(input)].getBurgerid()].getIngredient(0))
                        {
                            cout << "\nIncorrect input :(\n";
                            system("pause");
                        }
                    }
                    // u is not neccessary to check because the loop will start again and run update automatically. r also.
                } while (ans != "r" && ans != "R");
                //end of processing order
            }
        orderUpdate(order_no, score, Order); // update order after every valid action
    } while (true);
}

void Settings()
{
    int input = 0;
    do {
        system("cls");
        cout << "*** Setting Menu ***" << endl;
        cout << "[1] Time limit [" << timelimit << " sec]" << endl;
        cout << "[2] Max number of orders [" << maxorder << "]"<< endl;
        cout << "[3] Shuffle Mode(0=OFF, 1=ON) [" << doShuffle << "]" << endl;
        cout << "[4] Return to Game Menu" << endl;
        cout << "*********************" << endl;
        cout << "Option (1 - 4): " << endl;
        cin >> input;

        if (!cin) {
            cin.clear();
            cin.ignore(1000,'\n'); //ignore inappropriate input (1000char?)
        }

        if (input == 1)
        {
            cout << "Please input the time limit (not more than 300sec): ";
            cin >> timelimit;
            while (timelimit > 300 || timelimit <= 0)
            {
                cout << "Invalid input. Please try again.\n\n";
                cout << "Please input the time limit (not more than 300sec): ";
                cin >> timelimit;
            }
            cout << "\nThe time limit was successfully changed." << endl;
            system("pause");
        }
        else if (input == 2)
        {
            cout << "Please input the maximum number of orders (1 - 50): ";
            cin >> maxorder;
            while (maxorder > 50 || maxorder < 1)
            {
                cout << "Invalid input. Please try again.\n\n";
                cout << "Please input the maximum number of orders (1 - 50): ";
                cin >> maxorder;
            }
            cout << "\nThe maximum number of orders was successfully changed." << endl;
            system("pause");
        }
        else if (input == 3)
        {
            cout << "Please input the Shuffle Mode value (0=OFF, 1=ON): ";
            cin >> doShuffle;
            while (doShuffle != 0 && doShuffle != 1)
            {
                cout << "Invalid input. Please try again.\n\n";
                cout << "Please input the Shuffle Mode value (0=OFF, 1=ON): ";
                cin >> doShuffle;
            }
            cout << "\nThe status of Shuffle Mode was successfully changed." << endl;
            system("pause");
        }
        else if (input == 4)
            break;
        else
        {
            system("cls");
            cout << "Invalid input. Please try again.\n\n";
            system("pause");
        }
    } while (input != 4);
}

void BurgerMenus()
{
    int input = 0;

    do {
        system("cls");
        cout << "**** Burger Menu *****" << endl;
        cout << "[1] Types of Burger" << endl;
        cout << "[2] Ingredient Symbols" << endl;
        cout << "[3] Return to Game Menu" << endl;
        cout << "**********************" << endl;
        cout << "Option (1 - 3): " << endl;
        cin >> input;

        if (!cin) {
            cin.clear();
            cin.ignore(1000,'\n'); //ignore inappropriate input (1000char?)
        }

        if (input == 1)
        {
            cout << endl;
            cout << "Type of burgers" << "\t\t\t" << "Ingredients" << "\t\t\t\t\t" << "Cooking time" << endl;
            for (int i = 1; i <= 5; i++)
            {
                if (i != 2)
                    cout << "Type " << i << ": " << Burger[i].getName() << "\t\t" << Burger[i].getIngredient(1)<< "\t\t" << Burger[i].getCookingTime() << endl;
                else
                    cout << "Type " << i << ": " << Burger[i].getName() << "\t\t" << Burger[i].getIngredient(1)<< "\t" << Burger[i].getCookingTime() << endl;
            }
            cout << endl;
            system("pause");
        }
        else if (input == 2)
        {
            cout << endl;
            cout << "Ingredients" << "\t" << "Symbol" << "\t\t" << "Ingredients" << "\t" << "Symbol" << endl;
            cout << endl;
            cout << "Bread" << "\t\t" << "B/b" << "\t\t" << "Tomato" << "\t\t" << "T/t" << endl;
            cout << "Cheese" << "\t\t" << "C/c" << "\t\t" << "Mushroom" << "\t" << "M/m" << endl;
            cout << "Beef" << "\t\t" << "F/f" << "\t\t" << "Egg" << "\t\t" << "E/e" << endl;
            cout << "Lettuce" << "\t\t" << "L/l" << "\t\t" << "Salmon" << "\t\t" << "S/s" << endl;
            cout << endl;
            system("pause");
        }
        else if (input == 3)
            break;
        else
        {
            system("cls");
            cout << "Invalid input. Please try again.\n\n";
            system("pause");
        }
    } while (input != 3);
}

void Instructions()
{
    system("cls"); // Tutorial Greeting
    cout << "Welcome!...\n\n";
    cout << "In this Tutorial, the time limit of all orders will be extended for tutorial purpose!\n\n";
    cout << "Normally, it will only takes about 2 - 4 minutes.\n\n";
    system("pause");

    int order_no = 5, score = 10, maxorder = 5, attempt = 1; //attempt is the step of the tutorial
    string input;
    OrderList Order[maxorder+1];

    for (int i = 1; i <= order_no; i++) //extending the timelimit (TUTORIAL USE ONLY!!!)
    {
        int random = 1 + rand() % 5;
        Order[i].setInfo(random, Burger[random].getName(), 600, Burger[random].getCookingTime());
    }

    do
    {
        system("cls");
        cout << "This is a Tutorial that teaches you how to play this game!\n\n";
        printOrderlist(order_no, score, Order);

        switch (attempt)
        {
            case 1: //Tutorial Step 1
                cout << "***\nThis is the interface inside the Order list.\nPlease enter [U] to update the order list.\n***\n\n";
                cout << "Enter [U] for update, [Q] for Quit, or [1-" << order_no << "] for order: ";
                cin >> input;
                while (input != "u" && input != "U")
                {
                    cout << "\nPlease enter [U] to update the order list.\n";
                    cout << "Enter [U] for update, [Q] for Quit, or [1-" << order_no << "] for order: ";
                    cin >> input;
                }
                attempt += 1;
                break;

            case 2: //Tutorial Step 2
                cout << "***\nGreat! Did you see that the remaining time has changed?\nNow enter [1] to process order 1.\n***\n\n";
                cout << "Enter [U] for update, [Q] for Quit, or [1-" << order_no << "] for order: ";
                cin >> input;
                while (input != "1")
                {
                    cout << "\n***Please enter [1] to process order 1.***\n" << endl;
                    cout << "Enter [U] for update, [Q] for Quit, or [1-" << maxorder << "] for order: ";
                    cin >> input;
                }
                attempt += 1;
                break;
            case 5: //Tutorial Step 5
                cout << "***\nGreat! You did it! Now look at the order you have just finished.\nThe status has changed into \"cooking\".\n\nNow wait for 15 second.";
                Sleep(3000);
                cout << "\n\nNormally, you will not just sit here and wait for the order to be finished.\n";
                Sleep(3000);
                cout << "When you play the game, you will need to spend this waiting time to finish other querying orders.\n";
                Sleep(3000);
                cout << "Since this is a tutorial, we don't need follow the rule exactly.";
                Sleep(3000);
                cout << "Rule is boring...Right?";

                cout << "\n\nNow enter [U] to update the order list.\n***\n\n";
                cout << "Enter [U] for update, [Q] for Quit, or [1-" << maxorder << "] for order: ";
                cin >> input;
                while (input != "u" && input != "U")
                {
                    cout << "\nPlease enter [U] to update the order list.\n";
                    cout << "Enter [U] for update, [Q] for Quit, or [1-" << order_no << "] for order: ";
                    cin >> input;
                }
                attempt += 1;
                break;
            case 6: //Tutorial Step 6
                cout << "***\nCan you see that? Your order are ready to serve now!\nNow enter [1] to deliver your order and receive the scores you deserve!\n***\n\n";
                cout << "Enter [U] for update, [Q] for Quit, or [1-" << order_no << "] for order: ";
                cin >> input;
                while (input != "1")
                {
                    cout << "\nPlease enter [1] deliver finished order!\n";
                    cout << "Enter [U] for update, [Q] for Quit, or [1-" << order_no << "] for order: ";
                    cin >> input;
                }
                attempt += 1;
                break;
            case 7: //Tutorial Step 7
                cout << "***\nCongratulation! 10 points was added into you score!\nNow enter [Q] to end this tutorial.\n***\n\n";
                cout << "Enter [U] for update, [Q] for Quit, or [1-" << maxorder << "] for order: ";
                cin >> input;
                while (input != "q" && input != "Q")
                {
                    cout << "\nYou would miss me, wouldn't you?\nDon't worry I will be here whenever you click into this tutorial.\nSo, please enter [Q] to end this tutorial.\n";
                    cout << "Enter [U] for update, [Q] for Quit, or [1-" << order_no << "] for order: ";
                    cin >> input;
                }
        }

        if (input >= "1" && input <= to_string(order_no) && Order[stoi(input)].getStatus() == "ready to serve")
        {
            for (int i = stoi(input); i <= order_no-1; i++)
                swapArr(Order, i);
            order_no -= 1;
            score += 10;
        }
        else if (input >= "1" && input <= to_string(order_no))
        {
            //start of processing order
            string ans = "";
            do
            {
                printProcessorder(input, Order);
                switch (attempt)
                {
                    case 3: //Tutorial Step 3
                        cout << "***\nThis is the interface when processing the order.\nPlease enter [U] to update the order information.\n***\n\n";
                        cout << "Please choose [U] for update, [R] for return, or\n";
                        cout << "type correct key list to start cooking: ";
                        cin >> ans;
                        while (ans != "u" && ans != "U")
                        {
                            cout << "\nPlease enter [U] to update the order information.\n";
                            cout << "Please choose [U] for update, [R] for return, or\n";
                            cout << "type correct key list to start cooking: ";
                            cin >> ans;
                        }
                        attempt += 1;
                        break;
                    case 4: // Tutorial Step 4
                        cout << "***\nGreat! Did you see that the remaining time has changed?\nNow enter the correct Burger Key List within the remaining time to process the order.\n***\n\n";
                        cout << "Please choose [U] for update, [R] for return, or\n";
                        cout << "type correct key list to start cooking: ";
                        cin >> ans;
                        while (ans != Burger[Order[stoi(input)].getBurgerid()].getIngredient(0) || Order[stoi(input)].getTime() <= 0)
                        {
                            cout << "\n***Please enter the correct Burger Key List within the time to process order.***\n" << endl;
                            cout << "Please choose [U] for update, [R] for return, or\n";
                            cout << "type correct key list to start cooking: ";
                            cin >> ans;
                        }
                        attempt += 1;
                        break;
                }

                if (!(ans == "u" || ans == "U") && !(ans == "r" || ans == "R"))
                {

                    if (Order[stoi(input)].getTime() > 0 && ans == Burger[Order[stoi(input)].getBurgerid()].getIngredient(0))
                    {
                        Order[stoi(input)].updateStatus('c');
                        ans = "r";
                    }
                }
                // u is not neccessary to check because the loop will start again and run update automatically. r also.
            } while (ans != "r" && ans != "R");
        }
        orderUpdate(order_no, score, Order);
    } while (input != "q" && input != "Q");

    system("cls");
    cout << "Extra information:\n\n";
    cout << "(1) Shuffle Mode is available in this Game\n\n";
    cout << "\tIf Shuffle Mode is turned ON, \n\tthe order of burger key will be shuffled every time when you enter the process order interface.\n";
    cout << "\tThe default status of Shuffle Mode is OFF. You can turn it ON in Game Setting.\n\n";
    cout << "Enjoy your game :D\n";
    cout << "\nGood Luck :D\n";
    system("pause");
}

void Credits()
{
    system("cls");
    cout << "This game is created by: (in alphabetical order)\n\n";
    cout << "Heung Chun Lok\t17021980A\t201B\n";
    cout << "Lai Tsun Wai\t17138632A\t201B\n";
    cout << "Ngai Kwok Fai\t18143326A\t201D\n";
    cout << "Wong Ho Yeung\t18140139A\t201B\n";
    cout << "Wong Wing Lam\t18019211A\t201D\n";
    cout << "Yim Chi Kit\t17059470A\t201C\n\n";
    system("pause");
    system("cls");
}

int main()
{
    int option = 0, shutdown =0;
    Burger[1].setInfo("Cheese burger", "bcflb", 10);
    Burger[2].setInfo("Beef burger", "bctflb", 10);
    Burger[3].setInfo("Mushroom burger","bcfmb", 15);
    Burger[4].setInfo("Veggie burger","btelb", 10);
    Burger[5].setInfo("Salmon burger", "bcfsb", 15);
    srand(time(0));

    do {
        system("cls");
        cout << "Welcome!\n\n";
        cout << "*** Game Menu ***\n";
        cout << "[1] Start Game\n";
        cout << "[2] Settings\n";
        cout << "[3] Burger Menus\n";
        cout << "[4] Instructions\n";
        cout << "[5] Credits\n";
        cout << "[6] Exit\n";
        cout << "*****************\n";
        cout << "Option (1 - 6):";
        cin >> option;

        if (!cin) {
            cin.clear();
            cin.ignore(1000,'\n'); //ignore inappropriate input (1000char?)
        }

        switch(option) {
            case 1: StartGame();
                break;
            case 2: Settings();
                break;
            case 3: BurgerMenus();
                break;
            case 4: Instructions();
                break;
            case 5: Credits();
                break;
            case 6: if (Exit())
                    shutdown=1;
                break;
            default:
                system("cls");
                cout << "Invalid input. Please try again." << endl;
                system("pause");
        }
    } while (shutdown != 1);

    return 0;
}