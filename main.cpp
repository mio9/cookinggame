// IDE: GroupAssignment.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Programmer: I know, thank you for your useful information

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <time.h>
#include <random>
#include <math.h>
#include <algorithm>
#include <windows.h> // tutorial usage

using namespace std;

// global settings variable:
int maxOrder = 5;
int timeLimit = 40;
bool advanced = false;
bool tutorMode = false;
string topmessage; // <- the notification slot on top

// global util vars
std::random_device rnd;

// util function for checking if a string is actually number
bool is_number(const std::string &s) {
    std::string::const_iterator it = s.begin();
    while (it != s.end() && isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

/**
 * Generate boolean base on fraction provided
 *
 * @param chance numerator
 * @param base denominator
 * @return boolean based on fraction provided
 */
bool fractionRand(int chance, int base) {
    int randfac = rnd() % base + 1;
    return randfac <= chance;
}

int rangeRand(int min, int max) {
    return min + (rnd() % (max - min + 1));
}

// global game vars
int score = 0;

//abstract method
bool getQuitConfirm();

/**
 * Represents a burger requested by the order
 */
class Burger {
private:

    string name;
    string recipe;
    int cookingTime;

public:
    /**
     * Construct a burger with a given type
     * @param type Type of the burger
     */
    Burger(int type) {
        switch (type) {
            case 1:
                this->name = "Cheese Burger";
                this->recipe = "bcflb";
                this->cookingTime = 10;
                break;
            case 2:
                // dis is da classic
                this->name = "Beef Burger";
                this->recipe = "bctblb";
                this->cookingTime = 10;
                break;
            case 3:
                this->name = "Mushroom Burger";
                this->recipe = "bcfmb";
                this->cookingTime = 15;
                break;
            case 4:
                // I'd rather not eating any burgers
                this->name = "Veggie Burger";
                this->recipe = "btelb";
                this->cookingTime = 10;
                break;
            case 5:
                // who would put salmon with beef?! that tastes bad!
                this->name = "Salmon Burger";
                this->recipe = "bcfsb";
                this->cookingTime = 10;
                break;
            default:
                cout << "NO you can't get to here";
        }
        // anti-social area
        if (advanced) {
            char ingredientList[] = {'b', 'c', 'f', 'l', 'm', 'e', 's'};
            // 3/5 chance modify first entry
            // 2/5 chance modify second time
            // 1/5 chance modify third time

            // 1/2 chance for both adding/removing item from the burger
            // (add item to random location)
            // (item can be anything, "bcfltmes")
            if (fractionRand(3, 5)) {
                this->modifyRecipe(ingredientList);
            } else if (fractionRand(2, 5)) {
                this->modifyRecipe(ingredientList);
            } else if (fractionRand(1, 5)) {
                this->modifyRecipe(ingredientList);
            }

        }

    }

    Burger() = default;


    string getName() const {
        return this->name;
    }

    string getRecipe() const {
        return this->recipe;
    }

    int getCookingTime() {
        return this->cookingTime;
    }
//
//    vector<string> getRecipeIngredients() const {
//
//    }

    string getIngredientString() const {
        string ingstr;
        vector<string> list;
        for (unsigned i = 0; i < this->recipe.length(); ++i) {
            char recipeCode = this->recipe.at(i);
            switch (recipeCode) {
                case 'b':
                    list.emplace_back("[B]read");
                    break;
                case 'c':
                    list.emplace_back("[C]heese");
                    break;
                case 'f':
                    list.emplace_back("Bee[f]");
                    break;
                case 'l':
                    list.emplace_back("[L]ettuce");
                    break;
                case 't':
                    list.emplace_back("[T]omato");
                    break;
                case 'm':
                    list.emplace_back("[M]ushroom");
                    break;
                case 'e':
                    list.emplace_back("[E]gg");
                    break;
                case 's':
                    list.emplace_back("[S]almon");
                    break;
            }
        }
        for (unsigned i = 0; i < list.size(); i++) {
            ingstr += list.at(i);

            // stop it from adding comma at the end
            if (i != list.size() - 1) {
                ingstr += ", ";
            }

        }

        return ingstr;
    }

private:
    void modifyRecipe(char ingredientList[]) {
        if (fractionRand(1, 2)) {
            this->recipe.erase(std::remove(this->recipe.begin(), this->recipe.end(), ingredientList[rnd() % 7]),
                               this->recipe.end());
        } else {
            this->recipe.insert(rangeRand(1, this->recipe.size() - 1), 1, ingredientList[rnd() % 7]);
        }
    }

};

class Order {
private:
    Burger burger;

    int orderTimeoutTime;
    int cookingCompleteTime;

public:
    /**
     * Construct an order with a specified burger
     * @param burger Burger object
     */
    Order(Burger burger) {
        this->burger = burger;
        this->orderTimeoutTime = time(0) + timeLimit;
        cookingCompleteTime = 0;
    }

    /**
     * Construct an order with random burger
     */
    Order() {
        this->burger = Burger(rnd() % 5 + 1);
        this->orderTimeoutTime = time(0) + timeLimit;
        cookingCompleteTime = 0;

    }

    const Burger &getBurger() const {
        return burger;
    }

    int getStatus() const {
        // 0 = Preparing , 1 = Cooking, 2 = Ready to serve , -1 = Timeout(not processed within time limit)
        if (orderTimeoutTime - time(0) < 0) {
            return -1;
        } else if (cookingCompleteTime == 0) {
            return 0;
        } else if (cookingCompleteTime - time(0) <= 0) {
            return 2;
        } else return 1;
    }

    string getStatusString() {
        switch (getStatus()) {
            case 0:
                return "preparing";
            case 1:
                return "cooking";
            case 2:
                return "ready to serve";
            case -1:
                // orders with -1 will be cleaned when doing update before this is diplayed
                return "timeout";
            default:
                //can't get to here unless someone made wrong stuff
                return nullptr;
        }
    }

    int getEndTime() const {
        return orderTimeoutTime;
    }

    int getRemainingSecond() {
        return this->orderTimeoutTime - time(0);
    }

    string getRemainingTimeString() {
        int remsec = this->orderTimeoutTime - time(0);
        if (remsec >= 0) {
            int min = floor(remsec / 60);
            int sec = remsec % 60;
            return to_string(min) + "\'" + to_string(sec) + "\"";
        } else {
            return "0\'0\"";
        }

    }

    int getCookingRemainingTime() const {
        return cookingCompleteTime - time(0);
    }

    string getCookingRemainingTimeString() const {
        int remsec = this->cookingCompleteTime - time(0);
        int min = floor(remsec / 60);
        int sec = remsec % 60;
        return to_string(min) + "\'" + to_string(sec) + "\"";
    }

    void cook() {
        //cook da burger
        this->cookingCompleteTime = time(0) + this->burger.getCookingTime();
    }

//    void setStatus(int newStatus) {
//        this->status = newStatus;
//    }


};


// global static variables:
vector<Order> orderList;

char showMainMenu() {
    char input;
    std::cout << "Welcome to Kurger Bing Simulator Extreme 2019" << std::endl;
    std::cout << std::endl;
    cout << "** Game Menu ***" << endl;
    cout << "[1] Start Game" << endl;
    cout << "[2] Settings" << endl;
    cout << "[3] Burger Menus" << endl;
    cout << "[4] Instructions" << endl;
    cout << "[5] Credits" << endl;
    cout << "[6] Exit" << endl;
    cout << "***************************" << endl;
    cout << "Option (1-6): ";

    cin >> input;
    cin.clear();
    return input;
}

/**
 *  Refreshes and update states of all orders in the order list
 *  (i.e. the function called when you press U)
 */
void refreshOrders() {

    if (score >= 0) {
        //generate new order{
        if (fractionRand(2, 5) && orderList.size() < maxOrder) {
            Order o = Order();
            orderList.push_back(o);
        }
        // remove timed out orders
        for (int i = 0; i < orderList.size(); i++) {
            Order &order = orderList.at(i);
            if (order.getStatus() == -1) {
                // remove timeout order
                orderList.erase(orderList.begin() + i);
                score -= 5;
            }
        }
    }

// update the cooking status of orders
    system("cls");
    cout << topmessage << endl;
    topmessage.clear();
    cout << "*** Order List ***" <<
         endl;
    if (score < 0) {
//congrat game over you're not going to make any more burger
        cout << "We are here to notify you that you are not doing your job quite well" <<
             endl << endl;
        cout << "[GAME OVER]" <<
             endl << endl;
        cout << "oops.. (=_=)" <<
             endl;
    } else {
        for (int i = 0; i < orderList.size(); i++) {
            cout << "Order " << "#" << i + 1 << ": " << orderList.at(i).getBurger().getName() << ", "
                 << orderList.at(i).getStatusString() << ", " << orderList.at(i).getRemainingTimeString() << endl;
        }
    }

    cout << "----------------------------------------------" << endl;
    cout << "Score: " << score << endl;
    if (!tutorMode) {
        cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: " << endl;
    }

}


/**
 *
 * @param orderNumber (display #number of the order)
 * @return magic integer - (1 = reward score, 0 = no action, -1 = deduct score)
 */
void processOrder(int orderNumber) {
    if (score < 0) {
        //wht do you expact when you get fired?
        return;
    }
    int index = orderNumber - 1;
    Order &order = orderList.at(index);

    if (order.getStatus() == 2) {
        // serve "ready to serve" order
        orderList.erase(orderList.begin() + index);
        score += 10;
        return;
    } else if (order.getStatus() == 1) {
        // the order is still cooking
        topmessage = "(!) The order is still cooking, please wait for it to finish ;)";
        return;
    }
    // main loop
    while (true) {
        system("cls");
        cout << topmessage << endl;
        topmessage.clear();
        cout << "*** Process Order ***" << endl;
        cout << left << setw(22) << "Order #" << ": " << orderNumber << endl;
        cout << left << setw(22) << "Burger" << ": " << order.getBurger().getName() << endl;
        cout << left << setw(22) << "Status" << ": " << order.getStatusString() << endl;
        cout << left << setw(22) << "Remaining Time" << ": " << order.getRemainingTimeString() << endl;
        cout << left << setw(22) << "Burger Ingredient List" << ": " << order.getBurger().getIngredientString() << endl;
        cout << left << setw(22) << "Burger Key List" << ": " << order.getBurger().getRecipe() << endl;

        cout << endl;
        string input;

        // when in tutorial mode, this part is ignored and implemented in startTutorGame()
        if (!tutorMode) {
            cout << "Please choose [U] for update, [R] for return, " << endl;
            cout << "or type correct key list to start cooking: ";

            cin >> input;
            cin.clear();
            if (input == "U" || input == "u") {
                // continue means run from top again
                system("cls");
                continue;

            } else if (input == "R" || input == "r") {
                // return means returning from the function -> quitting the function (get out of the loop)
                return;
            } else if (input == order.getBurger().getRecipe()) {
                // if the input is the keylist of berger ingredient
                order.cook();
                return;
            } else {
                topmessage = "(!) Incorrect input!";
                continue;
            }
        } else {
            break;
        }

    }


}


/**
 * Start the main game loop;
 * @param tutorMode TRUE if you want to run in tutor mode
 */
void startGame() {
    // clear the last game data
    score = 10;
    orderList.clear();

    system("cls");
    cout << endl;
    cout << endl;
    // Get you mentally ready for this torture you created by yourself
    //  Who told you to make extreme settings? (* ^w^)/


    // clean the screen
    system("cls");
    // generate some burgers first
    int initOrderAmount = rnd() % maxOrder + 1;
    for (int i = 0; i < initOrderAmount; i++) {
        Order o = Order();

        orderList.push_back(o);
    }


    while (true) {


        // DIsplay/refresh order list
        refreshOrders();
        string input;

        // let you quit directly
        if (score < 0) {
            cout << "(i) Press any key to return to menu";
            system("pause >nul");
            break;
        }


        cin >> input;
        cin.clear();
        if (input == "U" || input == "u") {
            //update
            continue;
        } else if (input == "Q" || input == "q") {
            if (getQuitConfirm()) {
                break;
            } else {
                continue;
            }
        } else if (is_number(input)) {
            if (stoi(input) - 1 < 0 || stoi(input) > orderList.size()) {
                topmessage = "(!) Your input is not proper";
                continue;
            }
            processOrder(stoi(input));
        } else {
            // shhh don't tell anyone about this refresh shortcut
            topmessage = "(!) Your input is not proper";
            continue;
        }
//        system("pause >nul");
    }


}

void startTutorGame() {
    // clear the last game data
    score = 10;
    orderList.clear();

    // mark the step of tutorial
    int step = 1;
    system("cls");
    string input;

// generate some burgers first
    int initOrderAmount = rnd() % maxOrder + 1;
    for (int i = 0; i < initOrderAmount; i++) {
        Order o = Order();
        orderList.push_back(o);
    }

    do {

        // clean the screen
        system("cls");

        refreshOrders();

        switch (step) {
            case 1://Tutorial Step 1
                cout
                        << "***\nThis is the interface inside the Order list.\nPlease enter [U] to update the order list.\n***\n\n";
                cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: ";
                cin >> input;
                while (input != "u" && input != "U") {
                    cout << "\nPlease enter [U] to update the order list.\n";
                    cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: ";
                    cin >> input;
                }
                step++;
                break;
            case 2: //Tutorial Step 2
                cout
                        << "***\nGreat! Did you see that the remaining time has changed?\nNow enter [1] to process order 1.\n***\n\n";
                cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: ";
                cin >> input;
                while (input != "1") {
                    cout << "\n***Please enter [1] to process order 1.***\n" << endl;
                    cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: ";
                    cin >> input;
                }
                step++;
                break;
            case 5: //Tutorial Step 5
                cout
                        << "***\nGreat! You did it! Now look at the order you have just finished.\nThe status has changed into \"cooking\".\n\nNow wait for 15 second.";
                Sleep(3000);
                cout << "\n\nNormally, you will not just sit here and wait for the order to be finished.\n";
                Sleep(3000);
                cout
                        << "When you play the game, you will need to spend this waiting time to finish other querying orders.\n";
                Sleep(3000);
                cout << "Since this is a tutorial, we don't need follow the rule exactly.";
                Sleep(4000);
                cout << "Rule is boring...Right?" << endl;

                cout << "\n\nNow enter [U] to update the order list.\n***\n\n";
                cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: ";
                cin >> input;
                while (input != "u" && input != "U") {
                    cout << "\nPlease enter [U] to update the order list.\n";
                    cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: ";
                    cin >> input;
                }
                step++;
                break;
            case 6: //Tutorial Step 6
                cout
                        << "***\nCan you see that? Your order are ready to serve now!\nNow enter [1] to deliver your order and receive the scores you deserve!\n***\n\n";
                cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: ";
                cin >> input;
                while (input != "1") {
                    cout << "\nPlease enter [1] deliver finished order!\n";
                    cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: ";
                    cin >> input;
                }
                step++;
                break;
            case 7: //Tutorial Step 7
                cout
                        << "***\nCongratulation! 10 points was added into you score!\nNow enter [Q] to end this tutorial.\n***\n\n";
                cout << "*************[ DEV'S  NOTE ]******************" << endl;
                cout << "***Check out the Advanced Mode[4] in Settings as well ;)" << endl;
                cout << "***Once enabled, customers will start to order their OWN flavour of burger" << endl;
                cout << "***Be sure to look \"carefully\" on what they ordered" << endl << endl;

                cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: ";
                cin >> input;
                while (input != "q" && input != "Q") {
                    cout
                            << "\nYou would miss me, wouldn't you?\nDon't worry I will be here whenever you click into this tutorial.\nSo, please enter [Q] to end this tutorial.\n";
                    cout << "Enter [U] for update, [Q] for Quit, or [1-" << orderList.size() << "] for order: ";
                    cin >> input;
                }

        }
        if (is_number(input)) {
            if (!(stoi(input) - 1 < 0 || stoi(input) > orderList.size())) {
                if (orderList.at(stoi(input) - 1).getStatus() == 2) {
                    // this function will automatically submit "ready to serve" orders
                    processOrder(stoi(input));
                } else {
                    // process the order
                    string ans;
                    do {
                        processOrder(stoi(input));
                        switch (step) {
                            case 3: // TUtorial Step 3
                                cout
                                        << "***\nThis is the interface when processing the order.\nPlease enter [U] to update the order information.\n***\n\n";
                                cout << "Please choose [U] for update, [R] for return, or\n";
                                cout << "type correct key list to start cooking: ";
                                cin >> ans;
                                while (ans != "u" && ans != "U") {
                                    cout << "\nPlease enter [U] to update the order information.\n";
                                    cout << "Please choose [U] for update, [R] for return, or\n";
                                    cout << "type correct key list to start cooking: ";
                                    cin >> ans;
                                }
                                step++;
                                break;
                            case 4: // Tutorial Step 4
                                cout
                                        << "***\nGreat! Did you see that the remaining time has changed?\nNow enter the correct Burger Key List within the remaining time to process the order.\n***\n\n";
                                cout << "Please choose [U] for update, [R] for return, or\n";
                                cout << "type correct key list to start cooking: ";
                                cin >> ans;
                                cin.clear();
                                while (ans != orderList.at(stoi(input) - 1).getBurger().getRecipe() ||
                                       orderList.at(stoi(input) - 1).getRemainingSecond() <= 0) {
                                    cout
                                            << "\n***Please enter the correct Burger Key List within the time to process order.***\n"
                                            << endl;
                                    cout << "Please choose [U] for update, [R] for return, or\n";
                                    cout << "type correct key list to start cooking: ";
//                                    cout << orderList.at(stoi(input)-1).getBurger().getRecipe();
                                    cin >> ans;
                                    cin.clear();
                                }
                                step++;
                                break;
                        }
                        if (!(ans == "u" || ans == "U") && !(ans == "r" || ans == "R")) {

                            if (orderList.at(stoi(input) - 1).getRemainingSecond() > 0 &&
                                ans == orderList.at(stoi(input) - 1).getBurger().getRecipe()) {
                                orderList.at(stoi(input) - 1).cook();
                                ans = "r";
                            }
                        }
                        refreshOrders();
                    } while (ans != "r" && ans != "R");
                }
            }
        }

        refreshOrders();

    } while (input != "q" && input != "Q");


}


void openSettings() {
    string rawinput;
    int input = 0;
    do {
        system("cls");
        cout << "*** Setting Menu ***" << endl;
        cout << "[1] Time limit [" << timeLimit << " sec]" << endl;
        cout << "[2] Max number of orders [" << maxOrder << "]" << endl;
        cout << "[3] Return to Game Menu" << endl << endl;
        cout << "[4] Advanced orders [" << advanced << "]" << "(0: Disabled, 1: Enabled)" << endl;
        cout << "or the \"anti-social mode\" called by the developers" << endl;
        cout << "*********************" << endl;
        cout << "Option (1 - 4): " << endl;
        cin >> rawinput;
        if (is_number(rawinput)) {
            input = stoi(rawinput);
        }
        if (input == 1) {
            cout << "Please input the time limit (not more than 300sec): ";
            string tempinp;
            cin >> tempinp;
            while (!is_number(tempinp) || stoi(tempinp) > 300 || stoi(tempinp) <= 0) {
                cout << "Invalid input. Please try again.\n\n";
                cout << "Please input the time limit (not more than 300sec): ";
                cin >> tempinp;
            }
            timeLimit = stoi(tempinp);
            cout << "\nThe time limit was successfully changed." << endl;
            cout << "(i) Press any key to back to continue";
            system("pause >nul");
        } else if (input == 2) {
            cout << "Please input the maximum number of orders (1 - 50): ";
            cin >> maxOrder;
            while (maxOrder > 50 || maxOrder < 1) {
                cout << "Invalid input. Please try again.\n\n";
                cout << "Please input the maximum number of orders (1 - 50): ";
                cin >> maxOrder;
            }
            cout << "\nThe maximum number of orders was successfully changed." << endl;
            cout << "(i) Press any key to back to continue";
            system("pause >nul");
        } else if (input == 4) {
            int adv_inp;
            cout << "Please choose to enable Advanced orders or not (0/1): ";
            cin >> adv_inp;
//            while (adv_inp > 1 || maxOrder < 0) {
//                cout << "Invalid input. Please try again.\n\n";
//                cout << "Please choose to enable Advanced orders or not (0/1): ";
//                cin >> adv_inp;
//            }
            advanced = adv_inp;
            cout << "\nThe maximum number of orders was successfully changed." << endl;
            cout << "(i) Press any key to back to continue";
            system("pause >nul");
        } else if (input == 3)
            break;
        else {
            system("cls");
            cout << "Invalid input. Please try again.\n\n";
            cout << "(i) Press any key to continue";
            system("pause");
        }
    } while (input != 3);
}

void showBurgerMenus() {


    char input;

    do {
        system("cls");
        cout << "**** Burger Menu *****" << endl;
        cout << "[1] Types of Burger" << endl;
        cout << "[2] Ingredient Symbols" << endl;
        cout << "[3] Return to Game Menu" << endl;
        cout << "**********************" << endl;
        cout << "Option (1 - 3): ";

        cin >> input;

        if (input == '1') {
            system("cls");
            cout << endl;
            cout << "******* [Burger Menu] ********" << endl;
            cout << "Type" << "\t\t\t" << left << setw(60) << "Ingredients" << setw(40) << "Cooking Time (sec)" << endl;
            cout
                    << "----------------------------------------------------------------------------------------------------------------"
                    << endl;
            for (int i = 1; i <= 5; i++) {
                Burger burg(i);
                string recipeVerbose = burg.getIngredientString();

                cout << burg.getName() << "\t\t" << left << setw(60) << recipeVerbose << setw(40)
                     << burg.getCookingTime()
                     << endl;


            }
            cout << "(i) Press any key to go back";
            system("pause >NUL");

        } else if (input == '2') {
            system("cls");
            cout << endl << endl;
            cout << "******* [Ingredient Symbols] ********" << endl;
            cout << endl;
            cout << "Ingredients" << "\t" << "Symbol" << "\t\t" << "Ingredients" << "\t" << "Symbol" << endl;
            cout << endl;
            cout << "Bread" << "\t\t" << "B/b" << "\t\t" << "Tomato" << "\t\t" << "T/t" << endl;
            cout << "Cheese" << "\t\t" << "C/c" << "\t\t" << "Mushroom" << "\t" << "M/m" << endl;
            cout << "Beef" << "\t\t" << "F/f" << "\t\t" << "Egg" << "\t\t" << "E/e" << endl;
            cout << "Lettuce" << "\t\t" << "L/l" << "\t\t" << "Salmon" << "\t\t" << "S/s" << endl;
            cout << endl;
            cout << "(i) Press any key to go back";
            system("pause >NUL");
        } else if (input == '3') {
            break;
        } else {
            system("cls");
            cout << "Invalid input. Please try again.\n\n";
            system("pause >NUL");
        }

    } while (input != '3');
//    cout << "Press any key to back to main menu";
//    system("pause >nul");
}

void runInstructions() {
    int originalTimeLimit = timeLimit;
    tutorMode = true;
    // setup settings for tutorial run
    timeLimit = 600;

    cout << "***** Instruction/Tutorial ********" << endl << endl;
    cout << "Hey, welcome to Kurger Bing! Looks like you're new to here...." << endl;
    cout << "I'm Karl the shop manager, come and try out your work flow (* ^-^)/" << endl << endl;
    cout << "(i) This tutorial will only take 2 to 4 minutes normally" << endl;
    cout << "";
    cout << "(i) Press any key to start the tutorial .." << endl;
    system("pause >nul");
    startTutorGame();

    // revert back the setting
    timeLimit = originalTimeLimit;
    tutorMode = false;
}

void showCredits() {
    cout << "Kurger Bing Simulator Extreme 2019" << endl;
    cout << "created by: (in alphabetical order)\n\n";

    cout << "Heung Chun Lok\t17021980A\t201B\n";
    cout << "Lai Tsun Wai\t17138632A\t201B\n";
    cout << "Ngai Kwok Fai\t18143326A\t201D\n";
    cout << "Wong Ho Yeung\t18140139A\t201B\n";
    cout << "Wong Wing Lam\t18019211A\t201D\n";
    cout << "Yim Chi Kit\t17059470A\t201C\n";

}

bool getQuitConfirm();

void test() {
    cout << "</ DEBUG - TEST PANEL >" << "\n\n";
    cout << time(0) << endl;
    cout << maxOrder << " | " << timeLimit << endl;
    int arr[5] = {0, 0, 0, 0, 0};
    for (int i = 0; i <= 5000; i++) {
        arr[rnd() % 5] += 1;
    }
    for (int q : arr) {
        cout << q << " ";
    }
    system("pause >nul");
    system("cls");
}

int main() {
    while (true) {
        char menuResponse = showMainMenu();
        system("cls");

        switch (menuResponse) {
            case '1':
                startGame();
                system("cls");
                break;
            case '2':
                openSettings();
                system("cls");
                break;
            case '3':
                showBurgerMenus();
                system("cls");
                break;
            case '4':
                runInstructions();
                system("cls");
                break;
            case '5':
                showCredits();
                cout << endl;
                cout << "(i) Press any key to back to main menu";
                system("pause >nul");
                system("cls");
                break;
            case '6':
                if (getQuitConfirm()) {
                    system("cls");
                    cout << "Thank you for playing :)" << endl;
//                    showCredits();
                    cout << "Press any key to quit the game";
//                    system("pause >nul");
                    system("cls");
                    exit(0);
                } else {
                    system("cls");
                }
                break;
            case '7':
                test();
            default:
                cout << "(!) Please enter a valid option code" << endl;
                break;
        }
    }


}

/**
 *
 * @return TRUE when want to quit, FALSE when return to previous page
 */
bool getQuitConfirm() {
    while (true) {
        char input;
        cout << "Do you really want to quit? (y/N)" << endl;
        cin >> input;
        cin.clear();

        if (input == 'y' || input == 'Y') {
            return true;
        } else if (input == 'n' || input == 'N') {
            return false;
        } else {
            system("cls");
            cout << "(!) Please input a proper response" << endl;
            continue;
        }

    }

}

